# -*- coding: utf-8 -*-
# Copyright 2019 Waffapo
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Salon de beauté, SPA',
    'summary': """
        Ce Logiciel intègre toutes les fonctionnalités indispensables à la gestion des activités de bien être : salon de beauté et spa :
""",
    'version': '10',
    'license': 'AGPL-3',
    'author': 'Waffapo,Odoo Community Association (OCA)',
    'website': 'www.waffapo.com',
    'depends': [
        'contact',
    ],
    'data': [
    ],
    'demo': [
    ],
}
